// The count-and-say sequence is a sequence of digit strings defined by the recursive formula:

// countAndSay(1) = "1"
// countAndSay(n) is the way you would "say" the digit string from countAndSay(n-1), which is then converted into a different digit string.
// To determine how you "say" a digit string, split it into the minimal number of substrings such that each substring contains exactly one unique digit. Then for each substring, say the number of digits, then say the digit. Finally, concatenate every said digit.

// Given a positive integer n, return the nth term of the count-and-say sequence.

 

// Example 1:

// Input: n = 1
// Output: "1"
// Explanation: This is the base case.
// Example 2:

// Input: n = 4
// Output: "1211"
// Explanation:
// countAndSay(1) = "1"
// countAndSay(2) = say "1" = one 1 = "11"
// countAndSay(3) = say "11" = two 1's = "21"
// countAndSay(4) = say "21" = one 2 + one 1 = "12" + "11" = "1211"
 

// Constraints:

// 1 <= n <= 30

class Solution {
public:
    static vector<string> getArrayOfSay (int n) {
        vector<string> res;
        res.push_back("1");
        //For 1 
        res.push_back("1");
        //For 2
        // res.push_back("11");
        for(int i =2;i<=n;i++){
            string digit = res[i-1];
            string resString = "";
            char currChar = digit[0];
            int currFreq = 1;
            if(digit.size() == 1){
                resString = resString+to_string(1)+ to_string(digit[0]-48);
                res.push_back(resString);
            }
            else{
            for(int i =1 ; i< digit.size(); i++){
                if ( currChar == digit[i]){
                    currFreq++;
                    if (i == digit.size()-1){
                        resString= resString+to_string(currFreq)+to_string((currChar-48));
                    }
                } else {
                    resString= resString+to_string(currFreq)+to_string((currChar-48));
                    currChar = digit[i];
                    currFreq = 1;
                    if (i == digit.size()-1){
                        resString= resString+to_string(currFreq)+to_string((currChar-48));
                    }
                    
                }
                
            }
            res.push_back(resString);
            }
        }
        return res;
        
    }
    
    string countAndSayOptimised(int n) {
        if(n == 1){
            return "1";
        }
        string s = countAndSay(n - 1);
        string ans = "";
        for(int i = 0; i<s.size(); i++){
            int count = 1;
            while(i < (s.size() - 1) and (s[i] == s[i + 1])){
                i++;
                count++;
            }
            ans.append(to_string(count) + s[i]);
        }
        return ans;
    }

    string countAndSay(int n){
        vector<string> res = getArrayOfSay(n);
        return res[n];
    }
};
