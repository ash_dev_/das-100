// You are climbing a staircase. It takes n steps to reach the top.

// Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

class Solution {
public:
    int climbStairsRecurssion(int n) {
            
        if(n == 0)
            return 1;
        if(n<0)
            return 0;
        return climbStairs(n-1)+climbStairs(n-2);
    }

    int climbStairsDP(int n){
        if(n<1){
            return 0;
        }
        int dp[100];
        dp[0] =1;
        dp[1] = 2;
        for(int i= 2; i<n ;i++){
            dp[i] = dp[i-1]+dp[i-2];
        }
        return dp[n-1];
        
    }
};
