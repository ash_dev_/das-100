// Online C++ compiler to run C++ program online
#include <iostream>
#include<bits/stdc++.h>
using namespace std;

int getFirstSegment(int x, int n){
    int res = x/int(pow(10,n/2));
    return res;
}

int getLaterSegment(int x, int n){
    int res = x% int(pow(10,n/2));
    return res;
}

int kMultiplication(int x, int y) {
    
    // Base Case
    if(x<10 || y<10)
        return x*y;
    
    
    cout<<"Entry Value"<<x <<" "<<y<<endl;
    int n = trunc(log10(x)) + 1;
    int a = getFirstSegment(x,n);
    int b = getLaterSegment(x,n);
    int c = getFirstSegment(y,n);
    int d = getLaterSegment(y,n);
    cout<<"Inital calc Value"<<n <<" "<<a<<" "<<b<<" "<<c<<" "<<d<<endl;
    int ac = kMultiplication(a,c);
    int bd = kMultiplication(b,d);
    int adbc = kMultiplication((a+b),(c+d)) - ac -bd;
    cout<<"Second step"<<" "<<ac<<" "<<bd<<" "<<adbc<<endl;
    int res = pow(10,n)*ac + pow(10,n/2)*adbc + bd;
    cout<<"Result "<<res<<endl;
    return res;


}

// main function to call the recussion function
int main() {
    // Write C++ code here
    cout<<kMultiplication(1234,5678);
    return 0;
}
