
// Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.

 

// Example 1:

// Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
// Output: [[1,6],[8,10],[15,18]]
// Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].
// Example 2:

// Input: intervals = [[1,4],[4,5]]
// Output: [[1,5]]
// Explanation: Intervals [1,4] and [4,5] are considered overlapping.

class Solution {
public:
  vector<vector<int>> merge(vector<vector<int>>& intr) {
      // sort the array so that the range are in ascending order
        sort(intr.begin(),intr.end());
      // push the first elemnt to the result.
      // idea is to compare the range of current with the last pushed element in the res
      // based on that either we we club them together or else we insert the current as separate entity
        vector<vector<int>> ans;
        ans.push_back(intr[0]);
        for(int i = 1; i < intr.size(); i++){
            if(intr[i][0]>ans[ans.size()-1][1]){
                ans.push_back(intr[i]);
            }else{
                ans[ans.size()-1][1] = max(intr[i][1],ans[ans.size()-1][1]);
            }
        }
        return ans;
    }
};
