
// Write a function to find the longest common prefix string amongst an array of strings.

// If there is no common prefix, return an empty string "".

 

// Example 1:

// Input: strs = ["flower","flow","flight"]
// Output: "fl"
// Example 2:

// Input: strs = ["dog","racecar","car"]
// Output: ""
// Explanation: There is no common prefix among the input strings.

class Solution {
public:
    string findCommomInTwo(string str1, string str2) {
        string res = "";
            // swap if str2 is greater in size than str1
            if(str2.length() > str1.length())
            {
                string temp = str1;
                str1 = str2;
                str2 = temp;
            }
            int pointer1 =0;
            int pointer2 = 0;
        if(str1[pointer1] == str2[pointer2])
        {
            while (pointer2 < str2.length())
            {
                cout<<"ptr1"<<" "<<pointer1<<" "<<str1[pointer1]<<"pt2 "<<pointer2<<" "<<str2[pointer2]<<endl;
                if (str1[pointer1] == str2[pointer2] && pointer1== pointer2)
                {
                    res+=str1[pointer1];
                    pointer1++;
                    pointer2++;
                }
                else
                    pointer2++;
                
            }
            }
        return res;
    }
    string longestCommonPrefix(vector<string>& strs) {
        string res = "";
        // return the whole string if length is 1
        if (strs.size() == 1)
            return strs[0];
        else {
        // first find common between first two stings
        res = findCommomInTwo(strs[0], strs[1]);
            cout<<res;
        for(int i =2; i< strs.size(); i++){
            res = findCommomInTwo(strs[i], res);
        }
            cout<<"Final res "<<res;
        }
        return res;
    }
};