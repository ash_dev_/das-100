
// Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

// A subarray is a contiguous part of an array.

 

// Example 1:

// Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
// Output: 6
// Explanation: [4,-1,2,1] has the largest sum = 6.
// Example 2:

// Input: nums = [1]
// Output: 1
// Example 3:

// Input: nums = [5,4,-1,7,8]
// Output: 23

// Brute Force
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        // brute force
        int maxSum = INT_MIN;
        for(int i =0 ; i< nums.size(); i++)
        {
            int currSum = 0;
            for(int j =i; j< nums.size() ;j++)
            {
                currSum += nums[j];
            if(currSum > maxSum)
                maxSum = currSum;
            }
        }
        return maxSum;
    }
};

// Optimal
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
     // A good read on Kadane's algo
    //https://medium.com/@rsinghal757/kadanes-algorithm-dynamic-programming-how-and-why-does-it-work-3fd8849ed73d
    //https://itnext.io/kadanes-algorithm-identify-pattern-12d96ea3de24
    // Basic logic is local_max_sum[i] = max (local_max_sum[i]+c, c)
    // where c is a number based on which we either take it as the starting point or include it with existing solution
        
        int local_max = 0;
        int global_max = INT_MIN;
        
        for(int i =0 ;i < nums.size(); i++){
            local_max = max (nums[i], nums[i]+local_max);
            global_max = max (global_max, local_max);
        }
        
        return global_max;
    }
};
