
// Given a string s consisting of words and spaces, return the length of the last word in the string.

// A word is a maximal substring consisting of non-space characters only.

 

// Example 1:

// Input: s = "Hello World"
// Output: 5
// Explanation: The last word is "World" with length 5.
// Example 2:

// Input: s = "   fly me   to   the moon  "
// Output: 4
// Explanation: The last word is "moon" with length 4.
// Example 3:

// Input: s = "luffy is still joyboy"
// Output: 6
// Explanation: The last word is "joyboy" with length 6.
 

// Constraints:

// 1 <= s.length <= 104
// s consists of only English letters and spaces ' '.
// There will be at least one word in s.

class Solution {
public:
    int lengthOfLastWord(string s) {
        bool isWhite = true;
        if(s.size() == 1)
            return 1;
        int lastEl = s.size()-1;
        // to get past the white spaces tpwards the end
        while(isWhite)
        {
            if(s[lastEl] == ' ')
                lastEl--;
            else
                isWhite = false;
        }
        cout<<"Last ele "<<lastEl<<endl;
        isWhite = false;
        // store the index of start of last element
        int lastEleStart = lastEl;
        if(lastEl == 0)
            return 1;
        //traverse to get to the start of last element
        while(!isWhite)
        {
            cout<<lastEl<<" ";
           if(lastEl>=0 && s[lastEl] != ' ' )
               lastEl--;
            else
                isWhite = true;
        }
        cout<<"Last ele start "<<lastEl;
        return lastEleStart-lastEl;
    }
};
