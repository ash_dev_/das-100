// Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

// You must implement a solution with a linear runtime complexity and use only constant extra space.

 

// Example 1:

// Input: nums = [2,2,1]
// Output: 1
// Example 2:

// Input: nums = [4,1,2,1,2]
// Output: 4
// Example 3:

// Input: nums = [1]
// Output: 1

//Method 1
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        if (nums.size() == 1)
            return nums[0];
        int res;
        unordered_map <int, bool> umap;
        for (int i =0; i< nums.size(); i++){
            if (umap.find(nums[i]) != umap.end())
            umap[nums[i]] = true;
            else
                umap[nums[i]] = false;
        }
        
            for (auto x : umap) {
                if (!x.second)
                    res = x.first;
            }
        return res;
        
    }
};

// Method 2
// Idea: A ^ A = 0
// A ^ B = 1
// So, all the repeating elemets will cancel out leaving only the elemts with one repetation
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        if(nums.size() == 1)
            return nums[0];
        int unique = nums[0];
        for (int i = 1; i < nums.size(); i++)
            unique = unique ^ nums[i];
        return unique;
        
    }
};

