
// A pangram is a sentence where every letter of the English alphabet appears at least once.

// Given a string sentence containing only lowercase English letters, return true if sentence is a pangram, or false otherwise.

 

// Example 1:

// Input: sentence = "thequickbrownfoxjumpsoverthelazydog"
// Output: true
// Explanation: sentence contains at least one of every letter of the English alphabet.
// Example 2:

// Input: sentence = "leetcode"
// Output: false

class Solution {
public:
    bool checkIfPangram(string s) {
        // array to track the letter frequency index wise
        int a[26] = {0};
        // traverse the string and increment the index in the arry
        for(int i = 0; i< s.length(); i++){
            a[s[i]-97]++;
        }
        bool res = true;
        // traverse through the array and check if each index is > 0
        for(int i = 0; i<26; i++){
            // cout<<i<<" "<<a[i]<<endl;
            if(a[i] == 0)
              res = false;
        }
        
        return res;
        
    }
};
