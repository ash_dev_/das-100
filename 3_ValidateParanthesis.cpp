
// Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

// An input string is valid if:

// Open brackets must be closed by the same type of brackets.
// Open brackets must be closed in the correct order.
// Every close bracket has a corresponding open bracket of the same type.
 

// Example 1:

// Input: s = "()"
// Output: true
// Example 2:

// Input: s = "()[]{}"
// Output: true
// Example 3:

// Input: s = "(]"
// Output: false

class Solution {
public:
    bool isValid(string s) {
        // return false for single element
        if (s.size() == 1) return false;
        // stack to do the vaidation check
        stack <char> pars;
        // loop through
        for (int i=0; i< s.size(); i++){
            char ele = char (s[i]);
            if ( ele == '(' || ele == '[' || ele == '{')
                pars.push(ele);
            else if  ( !pars.empty() && ((ele == ')' && pars.top() == '(') || (ele == ']' && pars.top() == '[') || (ele == '}' && pars.top() == '{')))
            {
                
                    pars.pop();
               
            }
              else return false;
        }
        
        return pars.empty();
        
    }
};