// Given two binary strings a and b, return their sum as a binary string.

 

// Example 1:

// Input: a = "11", b = "1"
// Output: "100"
// Example 2:

// Input: a = "1010", b = "1011"
// Output: "10101"
 

// Constraints:

// 1 <= a.length, b.length <= 104
// a and b consist only of '0' or '1' characters.
// Each string does not contain leading zeros except for the zero itself.

class Solution {
public:
    string addBinary(string a, string b) {
        if(b.length()>a.length())
        {
            string temp = a;
            a = b;
            b = temp;
        }
        int lengthDiff = a.size() - b.size();
        for(int i = 0 ;i <lengthDiff; i++)
            b="0"+b;
        int carry = 0;
        string res="";
        for(int i =a.size()-1 ; i>=0 ; i--)
        {
            int resVal = (a[i]-48) + (b[i]-48) + carry;
            carry = (resVal / 2);
            resVal = resVal % 2;
            res= to_string(resVal)+res;
            resVal = 0;
        }
        if (carry == 1)
            res=to_string(carry)+res;
        return res;
    }
};