
// Given an integer x, return true if x is palindrome integer.

// An integer is a palindrome when it reads the same backward as forward.

// For example, 121 is a palindrome while 123 is not.

// Example 1:

// Input: x = 121
// Output: true
// Explanation: 121 reads as 121 from left to right and from right to left.
// Example 2:

// Input: x = -121
// Output: false
// Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.

class Solution {
public:
    bool isPalindrome(long x) {
        if( x< 0)
            return false;
        long n =x;
        long res = 0;
        while( x != 0)
        {
            res = (res* 10) + (x%10);
            x = x/10;
            
        }
        
        return res == n;
        
    }
};