
// Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

// Symbol       Value
// I             1
// V             5
// X             10
// L             50
// C             100
// D             500
// M             1000
// For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.

// Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

// I can be placed before V (5) and X (10) to make 4 and 9. 
// X can be placed before L (50) and C (100) to make 40 and 90. 
// C can be placed before D (500) and M (1000) to make 400 and 900.
// Given an integer, convert it to a roman numeral.

 

// Example 1:

// Input: num = 3
// Output: "III"
// Explanation: 3 is represented as 3 ones.
// Example 2:

// Input: num = 58
// Output: "LVIII"
// Explanation: L = 50, V = 5, III = 3.
// Example 3:

// Input: num = 1994
// Output: "MCMXCIV"
// Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 
 class Solution {
public:
    string intToRoman(int n) {
        
        unordered_map<int, string> romanChar;
        romanChar[1] = "I";
        romanChar[4] = "IV";
        romanChar[5] = "V";
        romanChar[9] = "IX";
        romanChar[10] = "X";
        romanChar[40] = "XL";
        romanChar[50] = "L";
        romanChar[90] = "XC";
        romanChar[100] = "C";
        romanChar[400] = "CD";
        romanChar[500] = "D";
        romanChar[900] = "CM";
        romanChar[1000] = "M";
        string s="";
        while(n>0){
            if(n>=1000)
            {s=s+romanChar[1000];
             n = n-1000;
              continue;
            }
            if(n>=900)
            {
                s=s+romanChar[900];
             n = n-900;
                 continue;
                
            }
               if(n>=500)
            {
                s=s+romanChar[500];
             n = n-500;
                    continue;
                
            }
               if(n>=400)
            {
                s=s+romanChar[400];
                n = n-400;
                    continue;
                
            }
                 if(n>=100)
            {
                s=s+romanChar[100];
                n = n-100;
                      continue;
                
            }
                  if(n>=90)
            {
                s=s+romanChar[90];
                n = n-90;
                       continue;
                
            }
                   if(n>=50)
            {
                s=s+romanChar[50];
                n = n-50;
                        continue;
                
            }
                if(n>=40)
            {
                s=s+romanChar[40];
                n = n-40;
                     continue;
                
            }
                  if(n>=10)
            {
                s=s+romanChar[10];
                n = n-10;
                continue;
                       continue;
                
            }
                     if(n>=9)
            {
                s=s+romanChar[9];
                n = n-9;
                          continue;
                
            }
                         if(n>=5)
            {
                s=s+romanChar[5];
                n = n-5;
                              continue;
                
            }
                          if(n>=4)
            {
                s=s+romanChar[4];
                n = n-4;
                               continue;
                
            }
                          if(n>=1)
            {
                s=s+romanChar[1];
                n = n-1;
                               continue;
                
            }
        }
       
        return s;
        
    }
};
