
// Given the head of a sorted linked list, delete all duplicates such that each element appears only once. Return the linked list sorted as well.

// Input: head = [1,1,2]
// Output: [1,2]

// Input: head = [1,1,2,3,3]
// Output: [1,2,3]

// Constraints:

// The number of nodes in the list is in the range [0, 300].
// -100 <= Node.val <= 100
// The list is guaranteed to be sorted in ascending order.

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        ListNode *tempHead = head;
        ListNode *startRepeat = head;
        ListNode *res = startRepeat;
        ListNode *stopRepeat = head;
        // contains no element
        if(tempHead == NULL)
            return NULL;
        //contains one element
        if(tempHead->next == NULL)
            return head;
        // Loop through the LL
        while( tempHead != NULL){
            //If next element is same as curr, increment stopRepeat
            if(tempHead->next != NULL && tempHead->val == tempHead->next->val)
            {
                    stopRepeat = tempHead->next;
      
            }
            // If next element is not same, point the startRepeat of next to the non repeating element
            else if( tempHead->next != NULL && tempHead->val != tempHead->next->val) {
                startRepeat->next = tempHead->next;
                startRepeat = startRepeat->next;
                stopRepeat->next = tempHead->next;
                stopRepeat = stopRepeat->next;
                
            }
            // If reached end of loop, check if the last elemnt is already there in non repeating LL
            else{
                // If not, then append it to the list
                if(startRepeat->val != tempHead->val)
                {
                    startRepeat->next = tempHead;
                }
                startRepeat->next = NULL;
            }
             tempHead = tempHead->next;
        }
        return res;
        
    }
};
